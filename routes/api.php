<?php

use Illuminate\Support\Facades\Route;
use \Illuminate\Http\Response;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['local'])->group(function () {

    ##### Redirect if route not found ####
    Route::fallback(function(){
        return response()->json(responseFormat(__('messages.request_not_found')),Response::HTTP_NOT_FOUND);
    });

    #### Authentication routes #####
    Route::prefix('auth')->group(function () {
        Route::post('/login', 'AuthController@login')->name('auth.login');
        Route::post('/register', 'AuthController@register')->name('auth.register');
        Route::post('/password/reset', 'AuthController@resetPassword')->name('auth.password.reset');
    });

    ################################# Routes for logged user ################################
    Route::middleware(['auth:sanctum'])->group(function () {
        #### Authentication routes #####
        Route::prefix('auth')->group(function () {
            Route::post('/logout', 'AuthController@logout')->name('auth.logout');
            Route::post('/password/update', 'AuthController@updatePassword')->name('auth.password.update');
        });

        #### Dashboard routes #####
        Route::group(['prefix' => 'dashboard', 'middleware' => ['admin']], function(){
            #### Card routes #####
            Route::prefix('card')->group(function () {
                Route::post('/store', 'CardController@store')->name('card.store');
                Route::post('/update', 'CardController@update')->name('card.update');
                Route::get('/list', 'CardController@list')->name('card.list');
                Route::delete('/delete', 'CardController@delete')->name('card.delete');
                #### Card category routes #####
                Route::prefix('category')->group(function () {
                    Route::post('/store', 'CategoryController@store')->name('card.category.store');
                    Route::post('/update', 'CategoryController@update')->name('card.category.update');
                    Route::get('/list', 'CategoryController@list')->name('card.category.list');
                    Route::delete('/delete', 'CategoryController@delete')->name('card.category.delete');
                });
            });

            #### Renewal routes #####
            Route::prefix('renewal')->group(function () {
                Route::post('/update-status', 'RenewalController@updateStatus')->name('renewal.updateStatus');
                Route::get('/list', 'RenewalController@adminList')->name('renewal.list');

                #### Renewal category route #####
                Route::prefix('category')->group(function () {
                    Route::post('/store', 'RenewalCategoryController@store')->name('renewal.category.store');
                    Route::post('/update', 'RenewalCategoryController@update')->name('renewal.category.update');
                    Route::get('/list', 'RenewalCategoryController@list')->name('renewal.category.list');
                    Route::get('/drop-down-list', 'RenewalCategoryController@dropDownList')->name('renewal.category.dropDownList');
                    Route::delete('/delete', 'RenewalCategoryController@delete')->name('renewal.category.delete');
                });

                #### Renewal category route #####
                Route::prefix('package')->group(function () {
                    Route::post('/store', 'PackageController@store')->name('renewal.package.store');
                    Route::post('/update', 'PackageController@update')->name('renewal.package.update');
                    Route::get('/drop-down-list', 'PackageController@dropDownList')->name('renewal.package.dropDownList');
                    Route::delete('/delete', 'PackageController@delete')->name('renewal.package.delete');
                });
            });

            #### Recharge routes #####
            Route::prefix('recharge')->group(function () {
                Route::post('/store', 'RechargeController@store')->name('recharge.store');
                Route::post('/update', 'RechargeController@update')->name('recharge.update');
                Route::get('/list', 'RechargeController@list')->name('recharge.list');
                Route::delete('/delete', 'RechargeController@delete')->name('recharge.delete');
            });

            #### Users routes #####
            Route::prefix('user')->group(function () {
                Route::post('/store', 'UserController@store')->name('user.store');
                Route::post('/update', 'UserController@update')->name('user.update');
                Route::get('/list', 'UserController@list')->name('user.list');
                Route::delete('/delete', 'UserController@delete')->name('user.delete');
            });

            #### Complaints routes #####
            Route::prefix('complaint')->group(function () {
                Route::post('/reply', 'ComplaintController@update')->name('user.complaint.update');
            });

            #### Slider routes #####
            Route::prefix('slider')->group(function () {
                Route::post('/store', 'SliderController@store')->name('Slider.store');
                Route::post('/update', 'SliderController@update')->name('Slider.update');
                Route::delete('/delete', 'SliderController@delete')->name('Slider.delete');
            });

            #### Setting routes #####
            Route::prefix('setting')->group(function () {
                Route::post('/store', 'SettingController@store')->name('Setting.store');
                Route::post('/update', 'SettingController@update')->name('Setting.update');
                Route::get('/details', 'SettingController@details')->name('Setting.details');
                Route::get('/list', 'SettingController@list')->name('Setting.list');
                Route::delete('/delete', 'SettingController@delete')->name('Setting.delete');
            });


        });

        #### Dashboard routes #####
        Route::group(['prefix' => 'website', 'middleware' => ['client']], function(){

            #### Users routes #####
            Route::prefix('user')->group(function () {
                #### Online payment image routes #####
                Route::prefix('online-payment-image')->group(function () {
                    Route::post('/upload', 'OnlinePaymentImageController@store')->name('user.onlinePaymentImage.store');
                });
                #### Complaints routes #####
                Route::prefix('complaint')->group(function () {
                    Route::post('/create', 'ComplaintController@store')->name('user.complaint.store');
                });
            });

            #### online payment images routes #####
            Route::prefix('online-payment-image')->group(function () {
                Route::post('/upload', 'OnlinePaymentImageController@store')->name('user.onlinePaymentImage.store');
                Route::get('/list', 'OnlinePaymentImageController@list')->name('user.onlinePaymentImage.list');
            });

            #### Renewal routes #####
            Route::prefix('renew')->group(function () {
                Route::post('/store', 'RenewalController@store')->name('renew.store');
                Route::post('/update', 'RenewalController@update')->name('renew.update');
                Route::get('/list', 'RenewalController@list')->name('renew.list');
                Route::get('/category-list', 'RenewalCategoryController@dropDownList')->name('renew.category.dropDownList');
                Route::get('/package-list', 'PackageController@list')->name('renew.package.list');

            });

            #### Renewal routes #####
            Route::prefix('recharge')->group(function () {
                Route::get('/list', 'CardController@list')->name('recharge.list');
                Route::get('/card-category-list', 'CategoryController@list')->name('recharge.category.list');
                Route::get('/card-list', 'CardController@dropDownList')->name('recharge.card.dropDownList');
                Route::post('/card-store', 'CardController@cardStore')->name('recharge.card.store');

            });

        });

        #### Online payment image routes #####
        Route::get('/online-payment-image/list', 'OnlinePaymentImageController@list')->name('onlinePaymentImage.list');
        Route::get('/complaint/list', 'ComplaintController@list')->name('complaint.list');
    });


    #### Setting routes #####
    Route::prefix('setting')->group(function () {
        Route::get('/details', 'SettingController@details')->name('Setting.details');
    });

    #### Card routes #####
    Route::prefix('card')->group(function () {
        Route::get('/list', 'CardController@dropDownList');
        #### Card category routes #####
        Route::prefix('category')->group(function () {
            Route::get('/list', 'CategoryController@list')->name('card.category.list');
        });
    });

    #### Slider routes #####
    Route::prefix('slider')->group(function () {
        Route::get('/list', 'SliderController@list')->name('Slider.list');
    });
});
