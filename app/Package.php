<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ['name','price','value','renewal_category_id'];

    // return renewal category
    public function renewalCategory(){
        return $this->belongsTo('App\RenewalCategory','renewal_category_id','id');
    }
}
