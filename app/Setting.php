<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    protected $fillable = ['terms_conditions_ar','terms_conditions_fr','policy_ar','policy_fr','email','phone','address_ar','address_fr'
        ,'about_ar','about_fr'];
    protected $appends = ['terms_conditions','policy','about'];
//    protected $hidden = ['terms_conditions_ar','terms_conditions_fr','policy_ar','policy_fr','address_ar','address_fr','about_ar','about_fr'];

    // get terms and conditions with current language
    public function getTermsConditionsAttribute(){
        $selectedTermsConditions = request()->has('local') ? 'terms_conditions_'.request()->has('local') : 'terms_conditions_ar';
        return $this->$selectedTermsConditions;
    }

    // get policy with current language
    public function getPolicyAttribute(){
        $selectedPolicy = request()->has('local') ? 'policy_'.request()->has('local') : 'policy_ar';
        return $this->$selectedPolicy;
    }

    // get address with current language
    public function getAddressAttribute(){
        $selectedAddress = request()->has('local') ? 'address_'.request()->has('local') : 'address_ar';
        return $this->$selectedAddress;
    }

    // get about with current language
    public function getAboutAttribute(){
        $selectedAbout = request()->has('local') ? 'about_'.request()->has('local') : 'about_ar';
        return $this->$selectedAbout;
    }

}
