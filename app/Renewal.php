<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renewal extends Model
{
    //
    protected $fillable = ['user_id','card_number','value','status','package_id'];

    protected $appends = ['status_word'];

    /**
     * Get the card's status word.
     *
     * @param  string  $value
     * @return string
     */
    public function getStatusWordAttribute()
    {
        switch ($this->status){
            case 0:
                return __('messages.pending_renewal');
            case 1:
                return __('messages.confirmed_renewal');
            case 2:
                return __('messages.cancelled_renewal');
        }
    }

    // return user
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    // return package
    public function package(){
        return $this->belongsTo('App\Package','package_id','id');
    }
}
