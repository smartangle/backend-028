<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlinePaymentImage extends Model
{
    protected $fillable = ['user_id','image'];

    // return user
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    // return full image path
    public function getImageAttribute($value)
    {
        return config('app.url') . \Storage::url($value);
    }
}
