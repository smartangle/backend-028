<?php

if(!function_exists('responseFormat')){
    // return error msg
    function responseFormat($msg){
        $error = new \stdClass();
        $error->message = $msg;
        return $error;
    }

    // upload image function
    function uploadImage($image,$dir){
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        \Storage::disk('local')->putFileAs($dir, $image, $imageName);
        return $dir.'/'.$imageName;
    }

}
