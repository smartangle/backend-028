<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recharge extends Model
{
    //
    protected $fillable = ['user_id','value'];


    // return user
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
