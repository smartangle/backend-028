<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','address','surname','shop_name','status','role'
    ];
    protected $appends = ['credit'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'password','remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the user's role.
     *
     * @param  string  $value
     * @return string
     */
    public function getRoleAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Get the user's recharges.
     *
     * @param  string  $value
     * @return string
     */
    public function recharges()
    {
        return $this->hasMany('App\Recharge','user_id','id');
    }

    /**
     * Get the user's purchases.
     *
     * @param  string  $value
     * @return string
     */
    public function cards()
    {
        return $this->hasMany('App\Card','user_id','id');
    }

    /**
     * Get the user's renewals.
     *
     * @param  string  $value
     * @return string
     */
    public function renewals()
    {
        return $this->hasMany('App\Renewal','user_id','id');
    }

    /**
     * Get the user's credit.
     *
     * @param  string  $value
     * @return string
     */
    public function getCreditAttribute()
    {
        return $this->recharges()->sum('value') - ($this->renewals()->sum('value') + $this->cards()->sum('value'));
    }

    /**
     * Get the user's surname.
     *
     * @param  string  $value
     * @return string
     */
    public function getSurNameAttribute($value)
    {
        return !is_null($value) ? $value : $this->name;
    }
}



