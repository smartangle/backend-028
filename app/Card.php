<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    //
    protected $fillable = ['category_id','number','value','status','user_id','name'];

    protected $appends = ['status_word'];

    /**
     * Get the card's name.
     *
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Get the card's status word.
     *
     * @param  string  $value
     * @return string
     */
    public function getStatusWordAttribute()
    {
        return $this->status == 0 ? __('messages.available') : __('messages.not_available');
    }

    /**
     * Get the card's user.
     *
     * @param  string  $value
     * @return string
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

}
