<?php


namespace App\Repositories\CategoryRepository;

use App\Category;
use App\Enum\Config;

class CategoryRepository
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * List of categories.
     *
     * @return \App\Category $category
     */
    public function list()
    {
        return $this->category->get();
    }

    /**
     * Store new category.
     *
     * @return \App\Category $category
     */
    public function store($data)
    {
        return $this->category->create($data);
    }

    /**
     * Update category.
     *
     * @return \App\Category $category
     */
    public function update($id,$data)
    {
        $this->category->where('id',$id)->update($data);
        return $this->getCategory('id',$id);
    }

    /**
     * Delete category.
     *
     * @return \App\Category $category
     */
    public function delete($id)
    {
        return $this->category->where('id',$id)->delete();
    }

    /**
     * Get category.
     *
     * @return \App\Category $category
     */
    public function getCategory($field,$value)
    {
        return Category::where($field,$value)->first();
    }

}
