<?php


namespace App\Repositories\RenewalRepository;

use App\Enum\Config;
use App\Renewal;

class RenewalRepository
{
    protected $renewal;

    public function __construct(Renewal $renewal)
    {
        $this->renewal = $renewal;
    }

    /**
     * List of renewals for admin.
     *
     * @return \App\Renewal $renewal
     */
    public function adminList()
    {
        return $this->renewal->paginate(Config::pagination);
    }

    /**
     * List of renewals.
     *
     * @return \App\Renewal $renewal
     */
    public function list($data)
    {
        return $this->renewal->where('user_id',$data['user_id'])->paginate(Config::pagination);
    }

    /**
     * Store new renewal.
     *
     * @return \App\Renewal $renewal
     */
    public function store($data)
    {
        return $this->renewal->create($data);
    }

    /**
     * Update renewal.
     *
     * @return \App\Renewal $renewal
     */
    public function update($id,$data)
    {
        $this->renewal->where('id',$id)->update($data);
        return $this->getRenewal('id',$id);
    }

    /**
     * Get renewal.
     *
     * @return \App\Renewal $renewal
     */
    public function getRenewal($field,$value)
    {
        return $this->renewal->where($field,$value)->first();
    }

    /**
     * Check renewal status.
     *
     * @return \App\Renewal $renewal
     */
    public function checkStatus($id)
    {
        return $this->getRenewal('id',$id)->status;
    }

}
