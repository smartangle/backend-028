<?php


namespace App\Repositories\PackageRepository;

use App\Package;
use App\Enum\Config;

class PackageRepository
{
    protected $package;

    public function __construct(Package $package)
    {
        $this->package = $package;
    }

    /**
     * List of packages.
     *
     * @return \App\Package $package
     */
    public function list()
    {
        return $this->package->paginate(Config::pagination);
    }

    /**
     * List of packages.
     *
     * @return \App\Package $package
     */
    public function dropDownList()
    {
        return $this->package->get();
    }

    /**
     * Store new package.
     *
     * @return \App\Package $package
     */
    public function store($data)
    {
        return $this->package->create($data);
    }

    /**
     * Update package.
     *
     * @return \App\Package $package
     */
    public function update($id,$data)
    {
        $this->package->where('id',$id)->update($data);
        return $this->getPackage('id',$id);
    }

    /**
     * Delete package.
     *
     * @return \App\Package $package
     */
    public function delete($id)
    {
        return $this->package->where('id',$id)->delete();
    }

    /**
     * Get package.
     *
     * @return \App\Package $package
     */
    public function getPackage($field,$value)
    {
        return Package::where($field,$value)->first();
    }

}
