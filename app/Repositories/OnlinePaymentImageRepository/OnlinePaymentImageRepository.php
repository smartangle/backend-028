<?php


namespace App\Repositories\OnlinePaymentImageRepository;

use App\OnlinePaymentImage;
use App\Enum\Config;

class OnlinePaymentImageRepository
{
    protected $onlinePaymentImage;

    public function __construct(OnlinePaymentImage $onlinePaymentImage)
    {
        $this->onlinePaymentImage = $onlinePaymentImage;
    }

    /**
     * List of payment images.
     *
     * @return \App\OnlinePaymentImage $onlinePaymentImage
     */
    public function list($id = null)
    {
        return $this->onlinePaymentImage->when(!is_null($id),function ($query) use ($id){
                                            $query->where('user_id',$id);
                                        })->paginate(Config::pagination);
    }

    /**
     * Store new payment image.
     *
     * @return \App\OnlinePaymentImage $paymentImage
     */
    public function store($data)
    {
        return $this->onlinePaymentImage->create($data);
    }

}
