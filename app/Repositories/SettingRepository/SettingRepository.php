<?php


namespace App\Repositories\SettingRepository;

use App\Setting;
use App\Enum\Config;

class SettingRepository
{
    protected $setting;

    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }

    /**
     * List of settings.
     *
     * @return \App\Setting $setting
     */
    public function list()
    {
        return $this->setting->get();
    }

    /**
     * Store new setting.
     *
     * @return \App\Setting $setting
     */
    public function store($data)
    {
        return $this->setting->create($data);
    }

    /**
     * Update setting.
     *
     * @return \App\Setting $setting
     */
    public function update($id,$data)
    {
        $this->setting->where('id',$id)->update($data);
        return $this->getSetting('id',$id);
    }

    /**
     * Delete setting.
     *
     * @return \App\Setting $setting
     */
    public function delete($id)
    {
        return $this->setting->where('id',$id)->delete();
    }

    /**
     * Get setting.
     *
     * @return \App\Setting $setting
     */
    public function getSetting($field,$value)
    {
        return Setting::where($field,$value)->first();
    }

}
