<?php


namespace App\Repositories\RechargeRepository;

use App\Recharge;
use App\Enum\Config;

class RechargeRepository
{
    protected $recharge;

    public function __construct(Recharge $recharge)
    {
        $this->recharge = $recharge;
    }

    /**
     * List of categories.
     *
     * @return \App\Recharge $recharge
     */
    public function list()
    {
        return $this->recharge->paginate(Config::pagination);
    }

    /**
     * Store new recharge.
     *
     * @return \App\Recharge $recharge
     */
    public function store($data)
    {
        return $this->recharge->create($data);
    }

    /**
     * Update recharge.
     *
     * @return \App\Recharge $recharge
     */
    public function update($id,$data)
    {
        $this->recharge->where('id',$id)->update($data);
        return $this->getRecharge('id',$id);
    }

    /**
     * Delete recharge.
     *
     * @return \App\Recharge $recharge
     */
    public function delete($id)
    {
        return $this->recharge->where('id',$id)->delete();
    }

    /**
     * Get recharge.
     *
     * @return \App\Recharge $recharge
     */
    public function getRecharge($field,$value)
    {
        return Recharge::where($field,$value)->first();
    }

}
