<?php


namespace App\Repositories\ComplaintRepository;

use App\Complaint;
use App\Enum\Config;

class ComplaintRepository
{
    protected $complaint;

    public function __construct(Complaint $complaint)
    {
        $this->complaint = $complaint;
    }

    /**
     * List of categories.
     *
     * @return \App\Complaint $complaint
     */
    public function list($id = null)
    {
        return $this->complaint->when(!is_null($id),function ($query) use ($id){
            $query->where('user_id',$id);
        })->paginate(Config::pagination);
    }

    /**
     * Store new complaint.
     *
     * @return \App\Complaint $complaint
     */
    public function store($data)
    {
        return $this->complaint->create($data);
    }

    /**
     * Update complaint.
     *
     * @return \App\Complaint $complaint
     */
    public function update($id,$data)
    {
        $this->complaint->where('id',$id)->update($data);
        return $this->getComplaint('id',$id);
    }

    /**
     * Delete complaint.
     *
     * @return \App\Complaint $complaint
     */
    public function delete($id)
    {
        return $this->complaint->where('id',$id)->delete();
    }

    /**
     * Get complaint.
     *
     * @return \App\Complaint $complaint
     */
    public function getComplaint($field,$value)
    {
        return Complaint::where($field,$value)->first();
    }

}
