<?php


namespace App\Repositories\SliderRepository;

use App\Slider;
use App\Enum\Config;

class SliderRepository
{
    protected $slider;

    public function __construct(Slider $slider)
    {
        $this->slider = $slider;
    }

    /**
     * List of sliders.
     *
     * @return \App\Slider $slider
     */
    public function list()
    {
        return $this->slider->get();
    }

    /**
     * Store new slider.
     *
     * @return \App\Slider $slider
     */
    public function store($data)
    {
        return $this->slider->create($data);
    }

    /**
     * Update slider.
     *
     * @return \App\Slider $slider
     */
    public function update($id,$data)
    {
        $this->slider->where('id',$id)->update($data);
        return $this->getSlider('id',$id);
    }

    /**
     * Delete slider.
     *
     * @return \App\Slider $slider
     */
    public function delete($id)
    {
        return $this->slider->where('id',$id)->delete();
    }

    /**
     * Get slider.
     *
     * @return \App\Slider $slider
     */
    public function getSlider($field,$value)
    {
        return Slider::where($field,$value)->first();
    }

}
