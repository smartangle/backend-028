<?php


namespace App\Repositories\CardRepository;

use App\Card;
use App\Enum\Config;

class CardRepository
{
    protected $card;

    public function __construct(Card $card)
    {
        $this->card = $card;
    }

    /**
     * List of cards.
     *
     * @return \App\Card $card
     */
    public function list($request)
    {
        return $this->card->when(strtolower($request->user()->role) == 'client',function ($query) use ($request){
                            $query->where('user_id',$request->user()->id)->where('status',1);
                        })->paginate(Config::pagination);
    }

    /**
     * List of cards.
     *
     * @return \App\Card $card
     */
    public function dropDownList($data)
    {
        return $this->card->where('category_id',$data['category_id'])
                    ->when(array_key_exists('credit',$data),function ($query) use ($data){
                        $query->where('value','<=',(int) $data['credit']);
                    })
                    ->where('status',0)->get();
    }

    /**
     * Store new card.
     *
     * @return \App\Card $card
     */
    public function store($data)
    {
        return $this->card->create($data);
    }

    /**
     * Update card.
     *
     * @return \App\Card $card
     */
    public function update($id,$data)
    {
        $this->card->where('id',$id)->update($data);
        return $this->getCard('id',$id);
    }

    /**
     * Delete card.
     *
     * @return \App\Card $card
     */
    public function delete($id)
    {
        return $this->card->where('id',$id)->delete();
    }

    /**
     * Get card.
     *
     * @return \App\Card $card
     */
    public function getCard($field,$value)
    {
        return Card::where($field,$value)->first();
    }


}
