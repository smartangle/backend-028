<?php


namespace App\Repositories\RenewalCategoryRepository;

use App\RenewalCategory;
use App\Enum\Config;

class RenewalCategoryRepository
{
    protected $renewalCategory;

    public function __construct(RenewalCategory $renewalCategory)
    {
        $this->renewalCategory = $renewalCategory;
    }

    /**
     * List of categories.
     *
     * @return \App\RenewalCategory $renewalCategory
     */
    public function list()
    {
        return $this->renewalCategory->paginate(Config::pagination);
    }

    /**
     * List of categories.
     *
     * @return \App\RenewalCategory $renewalCategory
     */
    public function dropDownList()
    {
        return $this->renewalCategory->get();
    }

    /**
     * Store new renewalCategory.
     *
     * @return \App\RenewalCategory $renewalCategory
     */
    public function store($data)
    {
        return $this->renewalCategory->create($data);
    }

    /**
     * Update renewalCategory.
     *
     * @return \App\RenewalCategory $renewalCategory
     */
    public function update($id,$data)
    {
        $this->renewalCategory->where('id',$id)->update($data);
        return $this->getRenewalCategory('id',$id);
    }

    /**
     * Delete renewalCategory.
     *
     * @return \App\RenewalCategory $renewalCategory
     */
    public function delete($id)
    {
        return $this->renewalCategory->where('id',$id)->delete();
    }

    /**
     * Get renewalCategory.
     *
     * @return \App\RenewalCategory $renewalCategory
     */
    public function getRenewalCategory($field,$value)
    {
        return RenewalCategory::where($field,$value)->first();
    }

}
