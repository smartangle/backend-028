<?php


namespace App\Repositories\UserRepository;


use App\Enum\Config;
use App\User;

class UserRepository
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Store new user.
     *
     * @return \App\User $user
     */
    public function store($data)
    {
        return $this->user->create($data);
    }

    /**
     * Update user.
     *
     * @return \App\User $user
     */
    public function update($id,$data)
    {
        return $this->user->where('id',$id)->update($data);
    }

    /**
     * Get specific user.
     *
     * @return \App\User $user
     */
    public function getUser($field,$value)
    {
        return $this->user->where($field,$value)->first();
    }


    /**
     * Delete user.
     *
     * @return boolean
     */
    public function delete($id)
    {
        return $this->user->where('id',$id)->delete($id);
    }

    /**
     * List of users.
     *
     * @return \App\Category $category
     */
    public function list()
    {
        return $this->user->paginate(Config::pagination);
    }

}
