<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RenewalCategory extends Model
{
    //
    protected $fillable = ['name'];

    /**
     * Get the card's name.
     *
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }
}
