<?php
namespace App\Services\ComplaintServices;

use App\Repositories\ComplaintRepository\ComplaintRepository;

class ComplaintService
{
    protected $complaintRepository;

    public function __construct(ComplaintRepository $complaintRepository)
    {
        $this->complaintRepository = $complaintRepository;
    }

    /**
     * List of catgeories.
     *
     * @return \App\Complaint $complaint
     */
    public function list($id = null)
    {
        return $this->complaintRepository->list($id);
    }

    /**
     * Store new complaint.
     *
     * @return \App\Complaint $complaint
     */
    public function store($data)
    {
        return $this->complaintRepository->store($data);
    }

    /**
     * Update complaint.
     *
     * @return \App\Complaint $complaint
     */
    public function update($id,$data)
    {
        return $this->complaintRepository->update($id,$data);
    }

    /**
     * Delete complaint.
     *
     * @return \App\Complaint $complaint
     */
    public function delete($id)
    {
        return $this->complaintRepository->delete($id);
    }

    /**
     * Get complaint.
     *
     * @return \App\Complaint $complaint
     */
    public function getComplaint($field,$value)
    {
        return $this->complaintRepository->getComplaint($field,$value);
    }


}
