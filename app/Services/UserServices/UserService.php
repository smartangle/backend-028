<?php
namespace App\Services\UserServices;

use App\Http\Resources\User\UserResource;
use App\Repositories\UserRepository\UserRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Store new user.
     *
     * @return \App\User $user
     */
    public function store($data)
    {
        return $this->userRepository->store($data);
    }

    /**
     * Update new user.
     *
     * @return \App\User $user
     */
    public function update($id,$data)
    {
        return $this->userRepository->update($id,$data);
    }

    /**
     * Get specific user details.
     *
     * @return \App\User $user
     */
    public function getUser($field,$value)
    {
        return $this->userRepository->getUser($field,$value);
    }

    /**
     * List of users.
     *
     * @return \App\Category $category
     */
    public function list()
    {
        return $this->userRepository->list();
    }

    /**
     * Delete user.
     *
     * @return boolean
     */
    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }

    /**
     * Auth response.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authResponse($user)
    {
        $accessToken = $user->createToken("castellumdz")->plainTextToken;
        $data['user'] = new UserResource($user);
        $data['access_token'] = $accessToken;
        return $data;
    }

    /**
     * Determine Auth field.
     *
     * @return string
     */
    public function checkAuthField($field)
    {
        return filter_var($field, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
    }

    /**
     * Fire old access tokens for user.
     *
     * @return string
     */
    public function fireOldAccessTokens($field,$value)
    {
        $user = $this->userRepository->getUser($field,$value);
        return DB::table('personal_access_tokens')->where('tokenable_id',$user->id)->delete();
    }

    /**
     * Check old password equal current password
     *
     * @return string
     */
    public function checkOldPassword($oldPassword,$currentPassword)
    {
        return Hash::check($oldPassword,$currentPassword);
    }

    /**
     * Hash password
     *
     * @return array
     */
    public function hasPassword($password)
    {
        return ['password'=>Hash::make($password)];
    }

}
