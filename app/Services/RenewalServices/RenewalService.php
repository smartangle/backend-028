<?php
namespace App\Services\RenewalServices;

use App\Repositories\RenewalRepository\RenewalRepository;
use App\Services\PackageServices\PackageService;

class RenewalService
{
    protected $renewalRepository,$packageService;

    public function __construct(RenewalRepository $renewalRepository,PackageService $packageService)
    {
        $this->renewalRepository = $renewalRepository;
        $this->packageService = $packageService;
    }

    /**
     * List of renewal.
     *
     * @return \App\Renewal $renewal
     */
    public function list($data)
    {
        return $this->renewalRepository->list($data);
    }

    /**
     * List of renewal for admin.
     *
     * @return \App\Renewal $renewal
     */
    public function adminList()
    {
        return $this->renewalRepository->adminList();
    }

    /**
     * Store new renewal.
     *
     * @return \App\Renewal $renewal
     */
    public function store($data)
    {
        $package = $this->packageService->getPackage('id',$data['package_id']);
        $data['value'] = $package->price;
        return $this->renewalRepository->store($data);
    }

    /**
     * Update renewal.
     *
     * @return \App\Renewal $renewal
     */
    public function update($id,$data)
    {
        $package = $this->packageService->getPackage('id',$data['package_id']);
        $data['value'] = $package->price;
        return $this->renewalRepository->update($id,$data);
    }

    /**
     * Get renewal.
     *
     * @return \App\Renewal $renewal
     */
    public function getRenewal($field,$value)
    {
        return $this->renewalRepository->getRenewal($field,$value);
    }

    /**
     * Check renewal status.
     *
     * @return \App\Renewal $renewal
     */
    public function checkStatus($id)
    {
        return $this->renewalRepository->checkStatus($id);
    }

}
