<?php
namespace App\Services\SettingServices;

use App\Http\Resources\Setting\SettingResource;
use App\Repositories\SettingRepository\SettingRepository;

class SettingService
{
    protected $settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    /**
     * List of catgeories.
     *
     * @return \App\Setting $setting
     */
    public function list()
    {
        return $this->settingRepository->list();
    }

    /**
     * Store new setting.
     *
     * @return \App\Setting $setting
     */
    public function store($data)
    {
        return $this->settingRepository->store($data);
    }

    /**
     * Update setting.
     *
     * @return \App\Setting $setting
     */
    public function update($id,$data)
    {
        return $this->settingRepository->update($id,$data);
    }

    /**
     * Delete setting.
     *
     * @return \App\Setting $setting
     */
    public function delete($id)
    {
        return $this->settingRepository->delete($id);
    }

    /**
     * Get setting.
     *
     * @return \App\Setting $setting
     */
    public function getSetting($field,$value)
    {
        return $this->settingRepository->getSetting($field,$value);
    }


}
