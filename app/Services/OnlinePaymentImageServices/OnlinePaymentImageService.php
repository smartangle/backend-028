<?php
namespace App\Services\OnlinePaymentImageServices;

use App\OnlinePaymentImage;
use App\Repositories\OnlinePaymentImageRepository\OnlinePaymentImageRepository;

class OnlinePaymentImageService
{
    protected $onlinePaymentImageRepository;

    public function __construct(OnlinePaymentImageRepository $onlinePaymentImageRepository)
    {
        $this->onlinePaymentImageRepository = $onlinePaymentImageRepository;
    }

    /**
     * List of online payment images.
     *
     * @return \App\OnlinePaymentImage $onlinePaymentImage
     */
    public function list($id = null)
    {
        return $this->onlinePaymentImageRepository->list($id);
    }

    /**
     * Store new payment image.
     *
     * @return \App\Category $category
     */
    public function store($data)
    {
        $data['image'] = $this->uploadImage($data['image']);
        return $this->onlinePaymentImageRepository->store($data);
    }

    /**
     * Upload image.
     *
     * @return string
     */
    public function uploadImage($image){
        return uploadImage($image,'onlinePayment');
    }


}
