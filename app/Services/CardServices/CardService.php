<?php
namespace App\Services\CardServices;

use App\Repositories\CardRepository\CardRepository;

class CardService
{
    protected $cardRepository;

    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    /**
     * List of cards.
     *
     * @return \App\Card $card
     */
    public function list($request)
    {
        return $this->cardRepository->list($request);
    }

    /**
     * List of cards.
     *
     * @return \App\Card $card
     */
    public function dropDownList($data)
    {
        return $this->cardRepository->dropDownList($data);
    }

    /**
     * Store new card.
     *
     * @return \App\Card $card
     */
    public function store($data)
    {
        return $this->cardRepository->store($data);
    }

    /**
     * Update card.
     *
     * @return \App\Card $card
     */
    public function update($id,$data)
    {
        return $this->cardRepository->update($id,$data);
    }

    /**
     * Delete card.
     *
     * @return \App\Card $card
     */
    public function delete($id)
    {
        return $this->cardRepository->delete($id);
    }

    /**
     * Get card.
     *
     * @return \App\Card $card
     */
    public function getCard($field,$value)
    {
        return $this->cardRepository->getCard($field,$value);
    }


}
