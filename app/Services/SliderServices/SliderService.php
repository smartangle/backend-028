<?php
namespace App\Services\SliderServices;

use App\Http\Resources\Slider\SliderResource;
use App\Repositories\SliderRepository\SliderRepository;

class SliderService
{
    protected $sliderRepository;

    public function __construct(SliderRepository $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;
    }

    /**
     * List of catgeories.
     *
     * @return \App\Slider $slider
     */
    public function list()
    {
        return $this->sliderRepository->list();
    }

    /**
     * Store new slider.
     *
     * @return \App\Slider $slider
     */
    public function store($data)
    {
        $data['path'] = uploadImage($data['image'],'slider');
        return $this->sliderRepository->store($data);
    }

    /**
     * Update slider.
     *
     * @return \App\Slider $slider
     */
    public function update($id,$data)
    {
        $data['path'] = uploadImage($data['image'],'slider');
        return $this->sliderRepository->update($id,$data);
    }

    /**
     * Delete slider.
     *
     * @return \App\Slider $slider
     */
    public function delete($id)
    {
        return $this->sliderRepository->delete($id);
    }

    /**
     * Get slider.
     *
     * @return \App\Slider $slider
     */
    public function getSlider($field,$value)
    {
        return $this->sliderRepository->getSlider($field,$value);
    }


}
