<?php
namespace App\Services\CategoryServices;

use App\Http\Resources\Category\CategoryResource;
use App\Repositories\CategoryRepository\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * List of catgeories.
     *
     * @return \App\Category $category
     */
    public function list()
    {
        return $this->categoryRepository->list();
    }

    /**
     * Store new category.
     *
     * @return \App\Category $category
     */
    public function store($data)
    {
        return $this->categoryRepository->store($data);
    }

    /**
     * Update category.
     *
     * @return \App\Category $category
     */
    public function update($id,$data)
    {
        return $this->categoryRepository->update($id,$data);
    }

    /**
     * Delete category.
     *
     * @return \App\Category $category
     */
    public function delete($id)
    {
        return $this->categoryRepository->delete($id);
    }

    /**
     * Get category.
     *
     * @return \App\Category $category
     */
    public function getCategory($field,$value)
    {
        return $this->categoryRepository->getCategory($field,$value);
    }


}
