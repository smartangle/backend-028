<?php
namespace App\Services\PackageServices;

use App\Repositories\PackageRepository\PackageRepository;

class PackageService
{
    protected $PackageRepository;

    public function __construct(PackageRepository $PackageRepository)
    {
        $this->PackageRepository = $PackageRepository;
    }

    /**
     * List of packages.
     *
     * @return \App\Package $Package
     */
    public function list()
    {
        return $this->PackageRepository->list();
    }

    /**
     * List of packages.
     *
     * @return \App\Package $Package
     */
    public function dropdownList()
    {
        return $this->PackageRepository->dropDownList();
    }

    /**
     * Store new Package.
     *
     * @return \App\Package $Package
     */
    public function store($data)
    {
        return $this->PackageRepository->store($data);
    }

    /**
     * Update Package.
     *
     * @return \App\Package $Package
     */
    public function update($id,$data)
    {
        return $this->PackageRepository->update($id,$data);
    }

    /**
     * Delete Package.
     *
     * @return \App\Package $Package
     */
    public function delete($id)
    {
        return $this->PackageRepository->delete($id);
    }

    /**
     * Get Package.
     *
     * @return \App\Package $Package
     */
    public function getPackage($field,$value)
    {
        return $this->PackageRepository->getPackage($field,$value);
    }


}
