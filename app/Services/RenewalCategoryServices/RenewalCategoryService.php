<?php
namespace App\Services\RenewalCategoryServices;

use App\Http\Resources\Category\CategoryResource;
use App\Repositories\RenewalCategoryRepository\RenewalCategoryRepository;

class RenewalCategoryService
{
    protected $renewalCategoryRepository;

    public function __construct(RenewalCategoryRepository $renewalCategoryRepository)
    {
        $this->renewalCategoryRepository = $renewalCategoryRepository;
    }

    /**
     * List of catgeories.
     *
     * @return \App\Category $renewalCategory
     */
    public function list()
    {
        return $this->renewalCategoryRepository->list();
    }

    /**
     * List of catgeories.
     *
     * @return \App\Category $renewalCategory
     */
    public function dropdownList()
    {
        return $this->renewalCategoryRepository->dropDownList();
    }

    /**
     * Store new renewalCategory.
     *
     * @return \App\Category $renewalCategory
     */
    public function store($data)
    {
        return $this->renewalCategoryRepository->store($data);
    }

    /**
     * Update renewalCategory.
     *
     * @return \App\Category $renewalCategory
     */
    public function update($id,$data)
    {
        return $this->renewalCategoryRepository->update($id,$data);
    }

    /**
     * Delete renewalCategory.
     *
     * @return \App\Category $renewalCategory
     */
    public function delete($id)
    {
        return $this->renewalCategoryRepository->delete($id);
    }

    /**
     * Get renewalCategory.
     *
     * @return \App\Category $renewalCategory
     */
    public function getCategory($field,$value)
    {
        return $this->renewalCategoryRepository->getCategory($field,$value);
    }


}
