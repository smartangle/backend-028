<?php
namespace App\Services\RechargeServices;

use App\Repositories\RechargeRepository\RechargeRepository;

class RechargeService
{
    protected $rechargeRepository;

    public function __construct(RechargeRepository $rechargeRepository)
    {
        $this->rechargeRepository = $rechargeRepository;
    }

    /**
     * List of catgeories.
     *
     * @return \App\Recharge $recharge
     */
    public function list()
    {
        return $this->rechargeRepository->list();
    }

    /**
     * Store new recharge.
     *
     * @return \App\Recharge $recharge
     */
    public function store($data)
    {
        return $this->rechargeRepository->store($data);
    }

    /**
     * Update recharge.
     *
     * @return \App\Recharge $recharge
     */
    public function update($id,$data)
    {
        return $this->rechargeRepository->update($id,$data);
    }

    /**
     * Delete recharge.
     *
     * @return \App\Recharge $recharge
     */
    public function delete($id)
    {
        return $this->rechargeRepository->delete($id);
    }

    /**
     * Get recharge.
     *
     * @return \App\Recharge $recharge
     */
    public function getRecharge($field,$value)
    {
        return $this->rechargeRepository->getRecharge($field,$value);
    }


}
