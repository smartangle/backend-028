<?php

namespace App\Http\Resources\Complaint;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ComplaintResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'details'=> $this->details,
            'reply'=> $this->reply,
            'user'=> new UserResource($this->user),
        ];
    }
}
