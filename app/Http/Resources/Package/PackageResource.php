<?php

namespace App\Http\Resources\Package;

use App\Http\Resources\RenewalCategory\RenewalCategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'name'=> $this->name,
            'value'=> $this->value,
            'price'=> $this->price,
            'category'=> new RenewalCategoryResource($this->renewalCategory),
        ];
    }
}
