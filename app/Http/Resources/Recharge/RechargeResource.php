<?php

namespace App\Http\Resources\Recharge;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RechargeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'value'=> $this->value,
            'total_value'=> $this->value + $this->user->credit,
            'user'=> new UserResource($this->user),
        ];
    }
}
