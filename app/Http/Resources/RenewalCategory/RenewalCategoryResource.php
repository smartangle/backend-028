<?php

namespace App\Http\Resources\RenewalCategory;

use Illuminate\Http\Resources\Json\JsonResource;

class RenewalCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'name'=> $this->name,
        ];
    }
}
