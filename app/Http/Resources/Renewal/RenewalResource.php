<?php

namespace App\Http\Resources\Renewal;

use App\Http\Resources\RenewalCategory\RenewalCategoryResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RenewalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'card_number'=> $this->card_number,
            'value'=> $this->value,
            'status'=> $this->status,
            'status_word'=> $this->status_word,
            'user'=> new UserResource($this->user),
            'renewal_category'=> new RenewalCategoryResource($this->renewalCategory),
        ];
    }
}
