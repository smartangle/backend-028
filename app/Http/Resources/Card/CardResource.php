<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'number'=> $this->number,
            'value'=> $this->value,
            'name'=> $this->name,
            'status'=> $this->status,
            'status_word'=> $this->status_word,
            'user'=> new UserResource($this->user),
        ];
    }
}
