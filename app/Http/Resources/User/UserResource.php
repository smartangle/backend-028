<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'name'=> $this->name,
            'email' => $this->email,
            'surname'=>$this->surname,
            'phone'=>$this->phone,
            'address'=>$this->address,
            'shop_name'=>$this->shop_name,
            'credit'=>$this->credit,
            'role'=>$this->role,
            'status'=>$this->status,
        ];
    }
}
