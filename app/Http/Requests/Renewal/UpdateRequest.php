<?php

namespace App\Http\Requests\Renewal;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric|exists:renewals,id',
            'card_number' => 'required|string',
            'package_id' => 'required|numeric|exists:packages,id',
        ];
    }
}
