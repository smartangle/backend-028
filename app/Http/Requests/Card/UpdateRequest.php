<?php

namespace App\Http\Requests\Card;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric|exists:cards,id',
            'category_id' => 'required|numeric|exists:categories,id',
            'user_id' => 'nullable|numeric|exists:users,id',
            'number' => 'required|string|unique:cards,number,'.request()->id,
            'status' => 'required|numeric',
            'value' => 'required|numeric|min:1',
            'name' => 'required|string',
        ];
    }
}
