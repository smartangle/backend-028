<?php

namespace App\Http\Controllers;

use App\Http\Requests\Package\DeleteRequest;
use App\Http\Requests\Package\StoreRequest;
use App\Http\Requests\Package\UpdateRequest;
use App\Http\Resources\Package\PackageResource;
use App\Package;
use App\Services\PackageServices\PackageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PackageController extends Controller
{
    private $packageService;

    public function __construct(PackageService $packageService)
    {
        $this->packageService = $packageService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function store(StoreRequest $request)
    {
        return new PackageResource($this->packageService->store($request->validated()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return object
     */
    public function update(UpdateRequest $request)
    {
        return new PackageResource($this->packageService->update($request->id,$request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $package
     * @return JsonResponse
     */
    public function delete(DeleteRequest $request)
    {
        $this->packageService->delete($request->id);
        return response()->json(responseFormat(__('messages.success_delete')), Response::HTTP_OK);

    }

    /**
     * Get list of package
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return array
     */
    public function list(Request $request)
    {
        return PackageResource::collection($this->packageService->list());
    }

    /**
     * Get list of package
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return array
     */
    public function dropDownList(Request $request)
    {
        return PackageResource::collection($this->packageService->dropdownList());
    }
}
