<?php

namespace App\Http\Controllers;

use App\Http\Requests\Authentication\LoginRequest;
use App\Http\Requests\Authentication\RegisterRequest;
use App\Http\Requests\Authentication\ResetPasswordRequest;
use App\Http\Requests\Authentication\UpdatePasswordRequest;
use App\Http\Resources\User\UserResource;
use App\Services\UserServices\UserService;
use App\User;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Regitser
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [file] image
     * @param  [string] password
     * @return [array] user data
     * @throws \SMartins\PassportMultiauth\Exceptions\MissingConfigException
     */
    public function register(RegisterRequest $request)
    {
        // create new user
        $this->userService->store($request->validated());
        // create user's access token
        $user = $this->userService->getUser('email',$request->email);
        $data = $this->userService->authResponse($user);
        return response()->json(['data' => $data],Response::HTTP_CREATED);

    }

    /**
     * Login
     *
     * @param  [string] email
     * @param  [string] password
     * @return [array] user data
     * @throws JsonEncodingException
     */
    public function login(LoginRequest $request)
    {
        $field = $this->userService->checkAuthField($request->provider);
        if (Auth::attempt([$field => $request->provider,'password' => $request->password])){
            $user = $this->userService->getUser($field,$request->provider);
            $data = $this->userService->authResponse($user);
            return response()->json(['data' => $data],Response::HTTP_OK);
        }
        return response()->json(responseFormat(__('messages.incorrect_email_or_password')),Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Reset Password
     *
     * @param  [string] email
     * @return [object] msg
     * @throws \SMartins\PassportMultiauth\Exceptions\MissingConfigException
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $this->userService->fireOldAccessTokens('email',$request->email);
        Password::sendResetLink($request->validated());
        return response()->json(responseFormat(__('auth.send_reset_password_link')), Response::HTTP_OK);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [object] msg
     */
    public function logout(Request $request){
        $request->user()->tokens()->delete();
        return response()->json(responseFormat(__('messages.logout_successfully')), Response::HTTP_OK);
    }

    /**
     * Update Password
     *
     * @param  [string] password
     * @param  [string] password confiramtion
     * @return [object] msg
     * @throws \SMartins\PassportMultiauth\Exceptions\MissingConfigException
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        if(!$this->userService->checkOldPassword($request->old_password,$request->user()->password))
            return response()->json(responseFormat(__('messages.old_password_is_incorrect')), Response::HTTP_FORBIDDEN);
        $this->userService->update($request->user()->id,$this->userService->hasPassword($request->password));
        return response()->json(responseFormat(__('messages.success_update')), Response::HTTP_OK);
    }

}
