<?php

namespace App\Http\Controllers;

use App\Complaint;
use App\Http\Requests\Complaint\StoreRequest;
use App\Http\Requests\Complaint\UpdateRequest;
use App\Http\Resources\Complaint\ComplaintResource;
use App\Services\ComplaintServices\ComplaintService;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    private $complaintService;

    public function __construct(ComplaintService $complaintService)
    {
        $this->complaintService = $complaintService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;
        return new ComplaintResource($this->complaintService->store($data));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        $data = $request->validated();
        $this->complaintService->update($request->id,$data);
        $complaint = Complaint::find($request->id);
        return new ComplaintResource($complaint);
    }

    /**
     * Get list of complaints
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $onlinePaymentImage
     * @return array
     */
    public function list(Request $request)
    {
        return ComplaintResource::collection($this->complaintService->list(strtolower($request->user()->role) == "client" ? $request->user()->id : null));
    }
}
