<?php

namespace App\Http\Controllers;

use App\Card;
use App\Http\Requests\Card\DeleteRequest;
use App\Http\Requests\Card\StoreRequest;
use App\Http\Requests\Card\UpdateRequest;
use App\Http\Requests\User\StoreUserCardRequest;
use App\Http\Resources\Card\CardResource;
use App\Services\CardServices\CardService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CardController extends Controller
{
    private $cardService;

    public function __construct(CardService $cardService)
    {
        $this->cardService = $cardService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        return new CardResource($this->cardService->getCard('id',$this->cardService->store($request->validated())->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function edit(Card $card)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        return new CardResource($this->cardService->update($request->id,$request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function delete(DeleteRequest $request)
    {
        $this->cardService->delete($request->id);
        return response()->json(responseFormat(__('messages.success_delete')), Response::HTTP_OK);
    }

    /**
     * Get list of cards
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return array
     */
    public function list(Request $request)
    {
        return CardResource::collection($this->cardService->list($request));
    }

    /**
     * Get list of cards
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return array
     */
    public function dropDownList(Request $request)
    {
        $data = $request->all();
        if(!is_null($request->user())){
            $data['credit'] = $request->user()->credit;
        }
        return CardResource::collection($this->cardService->dropDownList($data));
    }

    /**
     * Store card for user
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return array
     */
    public function cardStore(StoreUserCardRequest $request)
    {
        $data['status'] = 1;
        $data['user_id'] = $request->user()->id;
        if($this->cardService->getCard('id',$request->card_id)->status == 1){
            return response()->json(responseFormat(__('messages.invalid_card')), Response::HTTP_FORBIDDEN);
        }
        if($this->cardService->getCard('id',$request->card_id)->value <= $request->user()->credit){
            $this->cardService->update($request->card_id,$data);
            return new CardResource($this->cardService->getCard('id',$request->card_id));
        }
        return response()->json(responseFormat(__('messages.not_enough_charge')), Response::HTTP_FORBIDDEN);

    }



}
