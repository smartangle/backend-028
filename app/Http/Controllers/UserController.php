<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\DeleteRequest;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Resources\Card\CardResource;
use App\Http\Resources\User\UserResource;
use App\Services\UserServices\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    private $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return new UserResource($this->userService->getUser('id',$this->userService->store($request->validated())->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        $this->userService->update($request->id,$request->validated());
        return new UserResource($this->userService->getUser('id',$request->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(DeleteRequest $request)
    {
        $this->userService->delete($request->id);
        return response()->json(responseFormat(__('messages.success_delete')), Response::HTTP_OK);
    }

    /**
     * Get list of users
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $users
     * @return array
     */
    public function list(Request $request)
    {
        return UserResource::collection($this->userService->list());
    }

}
