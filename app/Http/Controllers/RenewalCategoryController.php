<?php

namespace App\Http\Controllers;

use App\Http\Requests\RenewalCategory\DeleteRequest;
use App\Http\Requests\RenewalCategory\StoreRequest;
use App\Http\Requests\RenewalCategory\UpdateRequest;
use App\Http\Resources\RenewalCategory\RenewalCategoryResource;
use App\Services\RenewalCategoryServices\RenewalCategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RenewalCategoryController extends Controller
{
    private $renewalCategoryService;

    public function __construct(RenewalCategoryService $renewalCategoryService)
    {
        $this->renewalCategoryService = $renewalCategoryService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function store(StoreRequest $request)
    {
        return new RenewalCategoryResource($this->renewalCategoryService->store($request->validated()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RenewalCategory  $renewalCategory
     * @return object
     */
    public function update(UpdateRequest $request)
    {
        return new RenewalCategoryResource($this->renewalCategoryService->update($request->id,$request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RenewalCategory  $renewalCategory
     * @return JsonResponse
     */
    public function delete(DeleteRequest $request)
    {
        $this->renewalCategoryService->delete($request->id);
        return response()->json(responseFormat(__('messages.success_delete')), Response::HTTP_OK);

    }

    /**
     * Get list of renewalCategory
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RenewalCategory  $renewalCategory
     * @return array
     */
    public function list(Request $request)
    {
        return RenewalCategoryResource::collection($this->renewalCategoryService->list());
    }

    /**
     * Get list of renewalCategory
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RenewalCategory  $renewalCategory
     * @return array
     */
    public function dropDownList(Request $request)
    {
        return RenewalCategoryResource::collection($this->renewalCategoryService->dropdownList());
    }
}
