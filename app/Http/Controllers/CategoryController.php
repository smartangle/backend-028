<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\Category\DeleteRequest;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Services\CategoryServices\CategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function store(StoreRequest $request)
    {
        return new CategoryResource($this->categoryService->store($request->validated()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return object
     */
    public function update(UpdateRequest $request)
    {
        return new CategoryResource($this->categoryService->update($request->id,$request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return JsonResponse
     */
    public function delete(DeleteRequest $request)
    {
        $this->categoryService->delete($request->id);
        return response()->json(responseFormat(__('messages.success_delete')), Response::HTTP_OK);

    }

    /**
     * Get list of category
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return array
     */
    public function list(Request $request)
    {
        return CategoryResource::collection($this->categoryService->list());
    }
}
