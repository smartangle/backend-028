<?php

namespace App\Http\Controllers;

use App\Http\Requests\Setting\StoreRequest;
use App\Http\Requests\Setting\UpdateRequest;
use App\Http\Resources\Setting\SettingResource;
use App\Services\SettingServices\SettingService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SettingController extends Controller
{
    private $settingService;

    public function __construct(SettingService $settingService)
    {
        $this->settingService = $settingService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function store(StoreRequest $request)
    {
        return new SettingResource($this->settingService->store($request->validated()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return object
     */
    public function update(UpdateRequest $request)
    {
        return new SettingResource($this->settingService->update($request->id,$request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->settingService->delete($request->id);
        return response()->json(responseFormat(__('messages.success_delete')), Response::HTTP_OK);

    }

    /**
     * Get list of setting
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return array
     */
    public function list(Request $request)
    {
        return SettingResource::collection($this->settingService->list());
    }

    /**
     * Get details
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return array
     */
    public function details(Request $request)
    {
        return new SettingResource($this->settingService->getSetting('id',1));
    }
}
