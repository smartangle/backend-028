<?php

namespace App\Http\Controllers;

use App\Http\Requests\Slider\StoreRequest;
use App\Http\Requests\Slider\UpdateRequest;
use App\Http\Resources\Slider\SliderResource;
use App\Services\SliderServices\SliderService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SliderController extends Controller
{
    private $sliderService;

    public function __construct(SliderService $sliderService)
    {
        $this->sliderService = $sliderService;
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function store(StoreRequest $request)
    {
        return new SliderResource($this->sliderService->store($request->validated()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return object
     */
    public function update(UpdateRequest $request)
    {
        return new SliderResource($this->sliderService->update($request->id,$request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->sliderService->delete($request->id);
        return response()->json(responseFormat(__('messages.success_delete')), Response::HTTP_OK);

    }

    /**
     * Get list of slider
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return array
     */
    public function list(Request $request)
    {
        return SliderResource::collection($this->sliderService->list());
    }
}
