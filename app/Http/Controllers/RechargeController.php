<?php

namespace App\Http\Controllers;

use App\Http\Requests\Recharge\DeleteRequest;
use App\Http\Requests\Recharge\StoreRequest;
use App\Http\Requests\Recharge\UpdateRequest;
use App\Http\Resources\Recharge\RechargeResource;
use App\Services\RechargeServices\ComplaintService;
use App\Services\RechargeServices\RechargeService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RechargeController extends Controller
{
    private $rechargeService;

    public function __construct(RechargeService $rechargeService)
    {
        $this->rechargeService = $rechargeService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        return new RechargeResource($this->rechargeService->getRecharge('id',$this->rechargeService->store($request->validated())->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recharge  $recharge
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        return new RechargeResource($this->rechargeService->update($request->id,$request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recharge  $recharge
     * @return \Illuminate\Http\Response
     */
    public function delete(DeleteRequest $request)
    {
        $this->rechargeService->delete($request->id);
        return response()->json(responseFormat(__('messages.success_delete')), Response::HTTP_OK);
    }

    /**
     * Get list of recharges
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recharge  $recharge
     * @return array
     */
    public function list(Request $request)
    {
        return RechargeResource::collection($this->rechargeService->list());
    }
}
