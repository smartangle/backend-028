<?php

namespace App\Http\Controllers;

use App\Http\Requests\Renewal\ListRequest;
use App\Http\Requests\Renewal\StoreRequest;
use App\Http\Requests\Renewal\UpdateRequest;
use App\Http\Requests\Renewal\UpdateStatusRequest;
use App\Http\Resources\Renewal\RenewalResource;
use App\Mail\RenewalMail;
use App\Renewal;
use App\Services\RenewalServices\RenewalService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

class RenewalController extends Controller
{
    private $renewalService;

    public function __construct(RenewalService $renewalService)
    {
        $this->renewalService = $renewalService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;
        return new RenewalResource($this->renewalService->getRenewal('id',$this->renewalService->store($data)->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Renewal  $renewal
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        $data = $request->validated();
        if ($this->renewalService->checkStatus($request->id) != 0)
            return response()->json(responseFormat(__('messages.cant_update_because_renewal_already_status',['status' =>
                $this->renewalService->checkStatus($request->id == 1) ? __('messages.confirmed_renewal') : __('messages.cancelled_renewal')])),Response::HTTP_FORBIDDEN);

        $data['user_id'] = $request->user()->id;
        return new RenewalResource($this->renewalService->update($request->id,$data));
    }

    /**
     * Get list of renewals
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Renewal  $renewal
     * @return array
     */
    public function list(Request $request)
    {
        $data['user_id'] = $request->user()->id;
        return RenewalResource::collection($this->renewalService->list($data));
    }

    /**
     * Get list of renewals for admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Renewal  $renewal
     * @return array
     */
    public function adminList(Request $request)
    {
        return RenewalResource::collection($this->renewalService->adminList());
    }

    /**
     * Update status the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Renewal  $renewal
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(UpdateStatusRequest $request)
    {

        if ($this->renewalService->checkStatus($request->id) != 0)
            return response()->json(responseFormat(__('messages.cant_update_because_renewal_already_status',['status' =>
                $this->renewalService->checkStatus($request->id == 1) ? __('messages.confirmed_renewal') : __('messages.cancelled_renewal')])),Response::HTTP_FORBIDDEN);

        $data = $request->validated();
        $data['package_id'] = $this->renewalService->getRenewal('id',$request->id)->package_id;

        $this->renewalService->update($request->id,$data);
        Mail::to($this->renewalService->getRenewal('id',$request->id)->user->email)->send(new RenewalMail($request->status ? __('messages.confirmed_renewal_mail',['number' => 33333 , 'value' => 300]) : __('messages.confirmed_renewal_mail',['number' => 222222])));

        return response()->json(responseFormat($request->status == 1 ? __('messages.confirmed_renewal')  : __('messages.cancelled_renewal')),Response::HTTP_FORBIDDEN);
    }
}
