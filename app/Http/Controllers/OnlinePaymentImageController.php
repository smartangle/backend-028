<?php

namespace App\Http\Controllers;

use App\Http\Requests\OnlinePaymentImage\StoreRequest;
use App\Http\Resources\OnlinePaymentImage\OnlinePaymentImageResource;
use App\OnlinePaymentImage;
use App\Services\OnlinePaymentImageServices\OnlinePaymentImageService;
use Illuminate\Http\Request;

class OnlinePaymentImageController extends Controller
{
    private $onlinePaymentImageService;

    public function __construct(OnlinePaymentImageService $onlinePaymentImageService)
    {
        $this->onlinePaymentImageService = $onlinePaymentImageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;
        return new OnlinePaymentImageResource($this->onlinePaymentImageService->store($data));
    }

    /**
     * Get list of onlinePaymentImage
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $onlinePaymentImage
     * @return array
     */
    public function list(Request $request)
    {
        return OnlinePaymentImageResource::collection($this->onlinePaymentImageService->list(strtolower($request->user()->role) == "client" ? $request->user() : null));
    }
}
