<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class ClientPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return strtolower($request->user()->role) == "client" ? $next($request) : response()->json(responseFormat(__('messages.not_available')),Response::HTTP_FORBIDDEN);
    }
}
