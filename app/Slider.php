<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Slider extends Model
{
    protected $fillable = ['path'];


    /**
     * Get user's image.
     */
    public function getPathAttribute($value)
    {
        if(is_null($value)){
            return asset('default-slider.jpg');
        }
        return config('app.url') . Storage::url($value);
    }
}
