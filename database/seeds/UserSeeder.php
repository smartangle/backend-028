<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create admin
        User::create([
            'name' => 'Support',
            'email' => 'support@castellumec.com',
            'password' => '123456',
            'phone' => '0123456789',
            'shop_name' => 'Admin Shop',
            'address' => 'Algeria',
            'role' => 'admin',
        ]);
    }
}
