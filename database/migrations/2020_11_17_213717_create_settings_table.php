<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->text('terms_conditions_ar')->nullable();
            $table->text('terms_conditions_fr')->nullable();
            $table->text('policy_ar')->nullable();
            $table->text('policy_fr')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('address_ar')->nullable();
            $table->text('address_fr')->nullable();
            $table->text('about_ar')->nullable();
            $table->text('about_fr')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
