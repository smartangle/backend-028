<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'success_request' => 'تم تنفيذ الطلب بنجاح',
    'failed_request' => 'فشل تنفيذ الطلب',
    'logout_successfully' => 'تم تسجيل الخروج بنجاح',
    'incorrect_email_or_password' => 'كلمة المرور او البريد الالكتروني او الهاتف غير صحيح',
    'request_not_found' => 'طلب غير موجود',
    'must_be_login' => 'برجاء تسجيل الدخول اوﻻ',
    'welcome' => 'مرحباً بك',
    'success_store'       => 'تمت الاضافة بنجاح',
    'failed_store'       => 'فشلت عملية الاضافة',
    'success_update'    => 'تم التعديل بنجاح',
    'success_moving'    => 'تم النقل بنجاح',
    'failed_update'       => 'فشلت عملية التعديل',
    'failed_moving'       => 'فشلت عملية النقل',
    'success_delete'    => 'تم الحذف بنجاح',
    'failed_delete'       => 'فشلت عملية الحذف',
    'reset_successfully'    => 'تم استعادة كلمة المرور بنجاح',
    'ok'                => 'موافق',
    'cancel'            => 'إلغاء',
    'close'            => 'إغلاق',
    'available'            => 'متاح',
    'not_enough_charge'            => 'رصيدك غير كافي',
    'not_available'            => 'غير متاح',
    'invalid_card'            => 'كارت غير صالح',
    'pending_renewal'            => 'جاري التجديد',
    'confirmed_renewal'            => 'تم التجديد',
    'cancelled_renewal'            => 'تم الغاء التجديد',
    'followed_successfully' => 'تم المتابعة بنجاح',
    'old_password_is_incorrect' => 'كلمة المرور القديمة غير صحيحة',
    'cant_update_because_renewal_already_status' => 'لا يمكنك التعديل لانه :status يمكنك انشاء تجديد اخر',
    'confirmed_renewal_mail' => 'تم تجديد اشتراك الجهاز رقم :number بقيمة :value',
    'cancelled_renewal_mail' => 'تم الغاء اشتراك الجهاز رقم :number ',
];
